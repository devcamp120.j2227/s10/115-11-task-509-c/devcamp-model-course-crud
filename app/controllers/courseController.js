// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module Course Model
const courseModel = require("../models/courseModel");

const getAllCourse = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    courseModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all course successfully",
            data: data
        })
    })
}

const createCourse = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     bodyTitle: 'Devcamp R2227',
    //     bodyDescription: 'NodeJS & ReactJs',
    //     bodyNoStudent: 25
    // }
    
    // B2: Validate dữ liệu
    // Kiểm tra title có hợp lệ hay không
    if(!body.bodyTitle) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Title không hợp lệ"
        })
    }

    // Kiểm tra noStudent có hợp lệ hay không
    if(isNaN(body.bodyNoStudent) || body.bodyNoStudent < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "No Student không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newCourse = {
        _id: mongoose.Types.ObjectId(),
        title: body.bodyTitle,
        description: body.bodyDescription,
        noStudent: body.bodyNoStudent
    }

    courseModel.create(newCourse, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(201).json({
            status: "Create course successfully",
            data: data
        })
    })
}

const getCourseById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    courseModel.findById(courseId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail course successfully",
            data: data
        })
    })
}

const updateCoureById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    if(body.bodyTitle !== undefined && body.bodyTitle.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Title không hợp lệ"
        })
    }

    if(body.bodyNoStudent !== undefined && ( isNaN(body.bodyNoStudent) || body.bodyNoStudent < 0 )) {
        return response.status(400).json({
            status: "Bad Request",
            message: "No Student không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateCourse = {}

    if(body.bodyTitle !== undefined) {
        updateCourse.title = body.bodyTitle
    }

    if(body.bodyDescription !== undefined) {
        updateCourse.description = body.bodyDescription
    }

    if(body.bodyNoStudent !== undefined) {
        updateCourse.noStudent = body.bodyNoStudent
    }

    courseModel.findByIdAndUpdate(courseId, updateCourse, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update course successfully",
            data: data
        })
    })
}

const deleteCourseByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete course successfully"
        })
    })
}

module.exports = {
    getAllCourse,
    createCourse,
    getCourseById,
    updateCoureById,
    deleteCourseByID
}